# Send an HTML email with an embedded image and a plain text message for
# email clients that don't want to display the HTML.
import pandas as pd

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage

import datetime
today = datetime.date.today().strftime('%Y%m%d')
#today = '20200701'

root_path = '/home/pi/dev/python/work/covid-19/'
data_path = root_path + 'data/'
filename1 = 'NJ_COVID-19_additional_cases_' + today + '.png'
filename2 = 'NE_COVID-19_additional_cases_' + today + '.png'
filename3 = 'US_COVID-19_additional_cases_' + today + '.png'


body = 'See attached:\n'
attachment1 = data_path + filename1
attachment2 = data_path + filename2
attachment3 = data_path + filename3

# Define these once; use them twice!
strFrom = 'Mathieu Bourg <mathieubourg@gmail.com>'
strTo = list(pd.read_csv(root_path + 'NJ_Leadership.csv')['Recipient'])

gmail_creds = pd.read_csv(root_path + 'gmail_creds.csv')
login = gmail_creds['address'][0]
passwd = gmail_creds['password'][0]

# Create the root message and fill in the from, to, and subject headers
msgRoot = MIMEMultipart('related')
msgRoot['Subject'] = '[COVID-19] Sitrep for NJ, NE and US as of ' + datetime.date.today().strftime('%d %b %Y')
msgRoot['From'] = strFrom
msgRoot['To'] = ', '.join(strTo)
# msgRoot.preamble = 'This is a multi-part message in MIME format.'

# Encapsulate the plain and HTML versions of the message body in an
# 'alternative' part, so message agents can decide which they want to display.
msgAlternative = MIMEMultipart('alternative')
msgRoot.attach(msgAlternative)

msgText = MIMEText('Hi,\nSee attached charts.')
msgAlternative.attach(msgText)

# We reference the image in the IMG SRC attribute by the ID we give it below
msgText = MIMEText('Hi,<br>See attached charts for today (source: The COVID Tracking Project - https://covidtracking.com).<br><img src="cid:image1"><br><img src="cid:image2"><br><img src="cid:image3"><br>', 'html')
msgAlternative.attach(msgText)

fp = open(attachment1, 'rb')
msgImage1 = MIMEImage(fp.read())
fp.close()
msgImage1.add_header('Content-ID', '<image1>')
msgRoot.attach(msgImage1)

fp = open(attachment2, 'rb')
msgImage2 = MIMEImage(fp.read())
fp.close()
msgImage2.add_header('Content-ID', '<image2>')
msgRoot.attach(msgImage2)

fp = open(attachment3, 'rb')
msgImage3 = MIMEImage(fp.read())
fp.close()
msgImage3.add_header('Content-ID', '<image3>')
msgRoot.attach(msgImage3)

# Send the email (this example assumes SMTP authentication is required)
import smtplib
smtp = smtplib.SMTP('smtp.gmail.com', 587)
smtp.starttls()
# smtp.connect('smtp.gmail.com', 587)
smtp.login(login, passwd)
smtp.sendmail(strFrom, strTo, msgRoot.as_string())
smtp.quit()

