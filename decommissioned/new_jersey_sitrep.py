#!/usr/bin/env python
# coding: utf-8

# ## Packages and environment setup

import datetime

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

today = datetime.date.today().strftime("%Y%m%d")
#today = '20200701'

root_folder = '/home/pi/dev/python/work/covid-19/'
repo = root_folder + 'data/'

# Data provided by "The COVID Tracking Project": https://covidtracking.com
daily_data = pd.read_json('http://covidtracking.com/api/states/daily')

# Data mangling
northeast_states = ['CT','DC','DE','MA','MD','ME','NH','NJ','NY','PA','RI','VA','VT','WV']
northeast_territories = ['VI','PR']
northeast = northeast_states + northeast_territories

states_daily_data = daily_data.copy()
states_daily_data.set_index('date', inplace=True)

def new_cases_table(state='US', skip=7):
    temp_table = states_daily_data.copy()
    if state == 'US':
        temp_table = temp_table.groupby('date').sum()
    elif state == 'NE':
        temp_table = temp_table[temp_table['state'].isin(northeast)].groupby('date').sum()
    else:
        temp_table = temp_table.query('state == @state').groupby('date').sum()
    label = 'positiveIncrease_' + str(skip) + 'd_average'
    temp_table[label] = temp_table.positiveIncrease.rolling(window=skip).mean()
    temp_table.loc[temp_table.loc[:, label] < 0, label] = 0
    temp_table.loc[temp_table.loc[:, 'positiveIncrease'] < 0, 'positiveIncrease'] = 0
    return temp_table, label

def plot_new_cases(state='US', skip=7):
    fig, ax1 = plt.subplots()
    ax2 = ax1.twiny()
    [temp_table, label] = new_cases_table(state, skip)
    temp_table.reset_index(inplace=True)
    temp_table['reference_date'] = pd.to_datetime(temp_table['date'].astype(str), format='%Y%m%d')
    title='Daily additional cases in ' + state + ' as of ' + today + '\n(' + str(skip) + ' days moving average in red)'
    temp_table.plot(x='reference_date', y=label, ax=ax1, color='red', figsize=(8,6), title=title)
    temp_table.plot.bar(y='positiveIncrease', ax=ax2, alpha=0.5)

    ax1.get_legend().remove()
    ax1.xaxis.set_label_text('')
    ax1.get_yaxis().set_major_formatter(ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
    ax2.set_xticklabels([])
    ax2.set_xticks([])
    ax2.get_legend().remove()

    plt.savefig(repo + state + '_COVID-19_additional_cases_' + today + '.png', bbox_inches='tight')

plot_new_cases('NJ')
plot_new_cases('NE')
plot_new_cases('US')

