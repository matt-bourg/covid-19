#!/usr/bin/env python
# coding: utf-8

# ## Packages and environment setup

import datetime

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

today = datetime.date.today().strftime("%Y%m%d")
#today = '20200701'

root_folder = '/home/pi/dev/python/work/covid-19/'
repo = root_folder + 'data/'

# Data provided by CDC: https://data.cdc.gov/Case-Surveillance/United-States-COVID-19-Cases-and-Deaths-by-State-o/9mfq-cb36
cdc_data_api_end_point = 'https://data.cdc.gov/resource/9mfq-cb36.json'
query_limit = '50000'

daily_data = pd.read_json(cdc_data_api_end_point + '?$limit=' + query_limit)

# Some setup for filtering on North Branch
northeast_states = ['CT','DC','DE','MA','MD','ME','NH','NJ','NY','PA','RI','VA','VT','WV']
northeast_territories = ['VI','PR']
northeast = northeast_states + northeast_territories
midwest = ['IA','IL','IN','KS','MI','MN','MO','NE','OH','WI']
north_branch = northeast + midwest

# Data preparation
states_daily_data = daily_data.copy()
states_daily_data['reference_date'] = pd.to_datetime(states_daily_data['submission_date'])
states_daily_data.drop(columns=['submission_date'], inplace=True)
states_daily_data.sort_values(by=['reference_date', 'state'], inplace=True)
states_daily_data.set_index('reference_date', inplace=True)

# Dirty fix for Jan 8th and 9th in NJ - new cases data manually interpolated
states_daily_data.set_index('state', inplace=True, append=True)
states_daily_data.loc[('2021-01-08', 'NJ'), 'new_case'] = 6674
states_daily_data.loc[('2021-01-09', 'NJ'), 'new_case'] = 6674
states_daily_data.reset_index(level = 'state', inplace=True)

def new_cases_table(state='US', skip=7):
    temp_table = states_daily_data.copy()
    if state == 'US':
        temp_table = temp_table.groupby('reference_date').sum()
    elif state == 'NB':
        temp_table = temp_table[temp_table['state'].isin(north_branch)].groupby('reference_date').sum()
    else:
        temp_table = temp_table.query('state == @state').groupby('reference_date').sum()
    label = 'new_cases_' + str(skip) + 'd_average'
    temp_table[label] = temp_table.new_case.rolling(window=skip).mean()
    temp_table.loc[temp_table.loc[:, label] < 0, label] = 0
    temp_table.loc[temp_table.loc[:, 'new_case'] < 0, 'new_case'] = 0
    return temp_table, label

def plot_new_cases(state='US', skip=7):
    _, ax1 = plt.subplots()
    ax2 = ax1.twiny()
    [temp_table, label] = new_cases_table(state, skip)
    temp_table.reset_index(inplace=True)
    title='Daily additional cases in ' + state + ' as of ' + today + '\n(' + str(skip) + ' days moving average in red)'
    temp_table.plot(x='reference_date', y=label, ax=ax1, color='red', figsize=(8,6), title=title)
    temp_table.plot.bar(y='new_case', ax=ax2, alpha=0.5)

    ax1.get_legend().remove()
    ax1.xaxis.set_label_text('')
    ax1.get_yaxis().set_major_formatter(ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
    ax2.set_xticklabels([])
    ax2.set_xticks([])
    ax2.get_legend().remove()

    plt.savefig(repo + state + '_CDC_COVID-19_additional_cases_' + today + '.png', bbox_inches='tight')

plot_new_cases('NJ')
plot_new_cases('NB')
plot_new_cases('US')
