FROM debian:stable

SHELL [ "/bin/bash", "--login", "-c" ]

RUN apt update && apt -y install wget

# Create a non-root user
ARG username=lagrange
ARG uid=1000
ARG gid=100
ENV USER $username
ENV UID $uid
ENV GID $gid
ENV HOME /home/$USER
RUN adduser --disabled-password \
    --gecos "Non-root user" \
    --uid $UID \
    --gid $GID \
    --home $HOME \
    $USER

COPY conda/requirements.txt conda/environment.yml /tmp/
RUN chown $UID:$GID /tmp/requirements.txt /tmp/environment.yml

# COPY conda/environment.yml /tmp/
# RUN chown $UID:$GID /tmp/environment.yml

# COPY postBuild /usr/local/bin/postBuild.sh
# RUN chown $UID:$GID /usr/local/bin/postBuild.sh && \
#     chmod u+x /usr/local/bin/postBuild.sh

COPY docker/entrypoint.sh /usr/local/bin/
RUN chown $UID:$GID /usr/local/bin/entrypoint.sh && \
    chmod u+x /usr/local/bin/entrypoint.sh

USER $USER
# install miniconda
# ENV MINICONDA_VERSION 4.9.2-7
ENV CONDA_DIR $HOME/miniconda3
RUN wget https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh -O ~/miniconda.sh && \
    chmod +x ~/miniconda.sh && \
    ~/miniconda.sh -b -p $CONDA_DIR && \
    rm ~/miniconda.sh
# make non-activate conda commands available
ENV PATH=$CONDA_DIR/bin:$PATH
# make conda activate command available from /bin/bash --login shells
RUN echo ". $CONDA_DIR/etc/profile.d/conda.sh" >> ~/.profile
# make conda activate command available from /bin/bash --interative shells
RUN conda init bash

# create a project directory inside user home
ENV PROJECT_DIR $HOME/app
RUN mkdir $PROJECT_DIR
WORKDIR $PROJECT_DIR

# build the conda environment
# ENV ENV_PREFIX $PWD/env
ENV ENV_NAME dev
RUN conda update --name base --channel defaults conda && \
    # conda env export --name base > /tmp/environment.yml && \
    # conda env create --name $ENV_NAME --file /tmp/environment.yml --force && \
    conda create --name $ENV_NAME --clone base --yes && \
    conda activate $ENV_NAME && conda install --yes --channel conda-forge --file /tmp/requirements.txt && conda deactivate && \
    conda clean --all --yes

# run the postBuild script to install any JupyterLab extensions
# RUN conda activate $ENV_NAME && 
#     /usr/local/bin/postBuild.sh && \
#     conda deactivate

ENTRYPOINT [ "/usr/local/bin/entrypoint.sh" ]

CMD [ "jupyter", "lab", "--no-browser", "--ip", "0.0.0.0" ]