#!/usr/bin/env python
# coding: utf-8

# ## Packages and environment setup

import datetime

import math
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

import geopandas as gpd
import geoplot as gpl
from geoplot import crs as gcrs
import mapclassify as mc

#import pyproj
#pyproj.datadir.set_data_dir = '/usr/local/share/proj'

today = datetime.date.today().strftime("%Y%m%d")
#today = '20200529'

root_folder = '/home/pi/dev/python/work/covid-19/'
repo = root_folder + 'data/'
#OneDrive_repo = '/Users/mathieu/OneDrive - Team Rubicon/COVID-19/'

northeast_states = ['CT','DC','DE','MA','MD','ME','NH','NJ','NY','PA','RI','VA','VT','WV']
northeast_territories = ['VI','PR']
northeast = northeast_states + northeast_territories


# Data provided by "The COVID Tracking Project": https://covidtracking.com
daily_data = pd.read_json('http://covidtracking.com/api/states/daily')


# Maps setup

# If maps already exists, load:
northeast_map = gpd.read_file(root_folder + 'TR_Northeast.geojson')
northeast_arranged_map = gpd.read_file(root_folder + 'TR_Northeast_arranged.geojson')

# Otherwise, create them and write files for later use. 
# Census files for states are available here: https://www2.census.gov/geo/tiger/TIGER2017//STATE/tl_2017_us_state.zip

# usa_map = gpd.read_file('../tl_2017_us_state/tl_2017_us_state.shp')
# northeast_states_map = usa_map[usa_map['STUSPS'].isin(northeast_states)]
# northeast_territories_map = usa_map[usa_map['STUSPS'].isin(northeast_territories)]
# northeast_map = usa_map[usa_map['STUSPS'].isin(northeast)]


# Move territories closer to Northeast for map rendering
# territories_shifted = northeast_territories_map.translate(xoff=-2, yoff=20)
# northeast_territories_map_shifted = northeast_territories_map.copy()
# northeast_territories_map_shifted['geometry'] = territories_shifted
# northeast_arranged_map = northeast_states_map.append(northeast_territories_map_shifted, ignore_index=True)

# northeast_map.to_file("TR_Northeast.geojson", driver='GeoJSON')
# northeast_arranged_map.to_file("TR_Northeast_arranged.geojson", driver='GeoJSON')


# Data mangling

states_daily_data = daily_data.copy()
states_daily_data.set_index('date', inplace=True)

northeast_states_daily_data_map = gpd.GeoDataFrame(states_daily_data.join(northeast_arranged_map.set_index('STUSPS'), on='state', how='inner'))

def new_cases_table(state='US', skip=7):
    temp_table = states_daily_data.copy()
    if state == 'US':
        temp_table = temp_table.groupby('date').sum()
    elif state == 'NE':
        temp_table = temp_table[temp_table['state'].isin(northeast)].groupby('date').sum()
    else:
        temp_table = temp_table.query('state == @state').groupby('date').sum()
    label = 'positiveIncrease_' + str(skip) + 'd_average'
    temp_table[label] = temp_table.positiveIncrease.rolling(window=skip).mean()
    temp_table.loc[temp_table.loc[:, label] < 0, label] = 0
    temp_table.loc[temp_table.loc[:, 'positiveIncrease'] < 0, 'positiveIncrease'] = 0
    return temp_table, label

def plot_new_cases(state='US', skip=7):
    fig, ax1 = plt.subplots()
    ax2 = ax1.twiny()
    [temp_table, label] = new_cases_table(state, skip)
    temp_table.reset_index(inplace=True)
    temp_table['reference_date'] = pd.to_datetime(temp_table['date'].astype(str), format='%Y%m%d')
    title='Daily additional cases in ' + state + ' as of ' + today + '\n(' + str(skip) + ' days moving average in red)'
    temp_table.plot(x='reference_date', y=label, ax=ax1, color='red', figsize=(8,6), title=title)
    temp_table.plot.bar(y='positiveIncrease', ax=ax2, alpha=0.5)

    ax1.get_legend().remove()
    ax1.xaxis.set_label_text('')
    ax1.get_yaxis().set_major_formatter(ticker.FuncFormatter(lambda x, p: format(int(x), ',')))
    ax2.set_xticklabels([])
    ax2.set_xticks([])
    ax2.get_legend().remove()

    plt.savefig(repo + state + '_COVID-19_additional_cases_' + today + '.png', bbox_inches='tight')

data=northeast_states_daily_data_map.query('date==@today')
scheme = mc.Quantiles(data['positive'],k=len(data['positive']))
legend_labels = [row['state'] + ': ' + str(math.floor(row['positive'])) for i, row in data.sort_values(by='positive', ascending=True).iterrows()]
ax=gpl.choropleth(data, projection=gcrs.WebMercator(), figsize=(8,8), hue='positive', scheme=scheme, cmap='Reds', edgecolor='lightgray', linewidth=1, legend=True, legend_labels=legend_labels, legend_kwargs={'loc': 'upper left'})
plt.title('Cases by state (as of ' + datetime.date.today().strftime('%d %b %Y') + ' 1830 hours ET)')
plt.savefig(repo + 'COVID-19_sitrep_'+today+'.png', bbox_inches='tight')
#plt.savefig(OneDrive_repo + 'COVID-19_sitrep_'+today+'.png', bbox_inches='tight')

# Vectorized if needed
# plt.savefig('/Users/mathieu/OneDrive - Team Rubicon/COVID-19/COVID-19_sitrep_'+today+'.pdf', bbox_inches='tight')

#northeast_daily_data = states_daily_data[states_daily_data['state'].isin(northeast)].groupby('date').sum()
#northeast_daily_data['positiveIncrease_7d_average'] = northeast_daily_data.positiveIncrease.rolling(window=7).mean()

#northeast_daily_data.plot.bar(y='positive', figsize=(8,6), title='Cases in Northeast (as of ' + datetime.date.today().strftime('%d %b %Y') + ')')
#ax = plt.axes()
#for i, label in enumerate(ax.xaxis.get_ticklabels()[::-1]):
#    if i % ticks_skip == 0:
#        label.set_visible(True)
#    else:
#        label.set_visible(False)

#plt.savefig(repo + 'NE_COVID-19_cases_'+today+'.png', bbox_inches='tight')

plot_new_cases('NE')

# northeast_daily_data.plot.bar(y='positiveIncrease_7d_average', figsize=(8,6), title='Daily additional cases in Northeast\n(7 days moving average as of ' + datetime.date.today().strftime('%d %b %Y') + ')')
# ax = plt.axes()
# for i, label in enumerate(ax.xaxis.get_ticklabels()[::-1]):
#     if i % ticks_skip == 0:
#         label.set_visible(True)
#     else:
#         label.set_visible(False)
# plt.savefig(repo + 'NE_COVID-19_additional_cases_7d_averaged_'+today+'.png', bbox_inches='tight')

f, axarr = plt.subplots(4, 4, figsize=(16,12), sharex=True, sharey=False)
j = 0
skip = 7
ticks_skip = 30

for i, row in data.sort_values(by='positive', ascending=False).iterrows():
    [temp_table, label] = new_cases_table(row['state'], skip)
    temp_table.sort_values(by='date')[label].plot.bar(ax=axarr[math.floor(j/4)][j%4])
    axarr[math.floor(j/4)][j%4].set_title(row['state'] + ' (' + str(math.floor(row['positive'])) + ' cases)')
    for index, mark in enumerate(axarr[math.floor(j/4)][j%4].xaxis.get_ticklabels()[::-1]):
        if index % ticks_skip == 0:
            mark.set_visible(True)
        else:
            mark.set_visible(False)
    j = j + 1

plt.savefig(repo + 'NE_COVID-19_additional_cases_by_state_7d_averaged_'+today+'.png', bbox_inches='tight')

