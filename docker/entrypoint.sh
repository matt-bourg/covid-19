#!/bin/bash --login
set -e
conda activate $ENV_NAME
exec "$@"