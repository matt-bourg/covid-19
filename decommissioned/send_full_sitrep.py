# Send an HTML email with an embedded image and a plain text message for
# email clients that don't want to display the HTML.
import pandas as pd

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage

import datetime
today = datetime.date.today().strftime('%Y%m%d')
#today = '20200625'

root_path = '/home/pi/dev/python/work/covid-19/'
data_path = root_path + 'data/'
filename1 = 'COVID-19_sitrep_' + today + '.png'
filename2 = 'NE_COVID-19_additional_cases_' + today + '.png'
#filename3 = 'NE_COVID-19_cases_' + today + '.png'
filename4 = 'NE_COVID-19_additional_cases_by_state_7d_averaged_' + today + '.png'

body = 'See attached:\n'
attachment1 = data_path + filename1
attachment2 = data_path + filename2
#attachment3 = data_path + filename3
attachment4 = data_path + filename4

# Define these once; use them twice!
strFrom = 'Mathieu Bourg <mathieubourg@gmail.com>'
strTo = list(pd.read_csv(root_path + 'recipients.csv')['Recipient'])

gmail_creds = pd.read_csv(root_path + 'gmail_creds.csv')
login = gmail_creds['address'][0]
passwd = gmail_creds['password'][0]

# Create the root message and fill in the from, to, and subject headers
msgRoot = MIMEMultipart('related')
msgRoot['Subject'] = '[COVID-19] Sitrep as of ' + datetime.date.today().strftime('%d %b %Y')
msgRoot['From'] = strFrom
msgRoot['To'] = ', '.join(strTo)
# msgRoot.preamble = 'This is a multi-part message in MIME format.'

# Encapsulate the plain and HTML versions of the message body in an
# 'alternative' part, so message agents can decide which they want to display.
msgAlternative = MIMEMultipart('alternative')
msgRoot.attach(msgAlternative)

msgText = MIMEText('Hi,\nSee attached sitrep.')
msgAlternative.attach(msgText)

# We reference the image in the IMG SRC attribute by the ID we give it below
msgText = MIMEText('Hi,<br>See attached sitrep.<br><img src="cid:image1"><br><img src="cid:image2"><br><img src="cid:image4"><br>', 'html')
msgAlternative.attach(msgText)

fp = open(attachment1, 'rb')
msgImage1 = MIMEImage(fp.read())
fp.close()
msgImage1.add_header('Content-ID', '<image1>')
msgRoot.attach(msgImage1)

fp = open(attachment2, 'rb')
msgImage2 = MIMEImage(fp.read())
fp.close()
msgImage2.add_header('Content-ID', '<image2>')
msgRoot.attach(msgImage2)

#fp = open(attachment3, 'rb')
#msgImage3 = MIMEImage(fp.read())
#fp.close()
#msgImage3.add_header('Content-ID', '<image3>')
#msgRoot.attach(msgImage3)

fp = open(attachment4, 'rb')
msgImage4 = MIMEImage(fp.read())
fp.close()
msgImage4.add_header('Content-ID', '<image4>')
msgRoot.attach(msgImage4)

# Send the email (this example assumes SMTP authentication is required)
import smtplib
smtp = smtplib.SMTP('smtp.gmail.com', 587)
smtp.starttls()
# smtp.connect('smtp.gmail.com', 587)
smtp.login(login, passwd)
smtp.sendmail(strFrom, strTo, msgRoot.as_string())
smtp.quit()

